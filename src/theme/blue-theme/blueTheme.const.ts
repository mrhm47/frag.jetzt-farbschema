export const blue = {


  '--primary' : '#00C9DB',
  '--primary-variant': '#FFFFFF',

  '--secondary': '#00C9DB',
  '--secondary-variant': '#373737',

  '--background': '#1b1b1b',
  '--surface': '#373737',
  '--dialog': '#373737',
  '--cancel': '#FFFFFF',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#000000',
  '--on-secondary': '#000000',
  '--on-background': '#FFFFFF',
  '--on-surface': '#FFFFFF',
  '--on-cancel': '#000000',

  '--green': 'lightgreen',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': '#ADADFF',
  '--purple': '#DE9AEA',
  '--light-green': '#85C125',
  '--grey': '#BDBDBD',
  '--grey-light': '#9E9E9E',
  '--black': '#212121',
  '--moderator': '#37474f'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Grey Mode',
      'de': 'Grey Mode'
    },
    'description': {
      'en': 'Color contrast compliant with WCAG 2.1 AA',
      'de': 'Farbkontrast nach WCAG 2.1 AA'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'surface'

};
